---
title: Home
draft: false
---

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
    </head>
    <body>
        <div class="section" id="button-anchor" style="scroll-margin-top: 3rem">
            <iframe src="https://www.surveymonkey.com/r/8GB65VP" width="100%" height="970" frameborder="0" marginheight="0" marginwidth="0">
                Loading…
            </iframe>
        </div>
        <div class="section">
            <div class="row">
                <div class="col text-center">
                    <a class="btn btn-primary btn-lg w-100 p-3" href="/liheap-kiosk/thank-you/"> Finish Survey</a>
                </div>
            </div>
        </div>
        <!-- Session expiration warning popup -->
        <div id="session-expiration-popup" class="popup">
            <div class="popup-content">
                <p>Your session is about to expire.</p>
                <p>Do you want to continue?</p>
                <div id="popup-idle-timer">Confirm before: <span id="popup-timer-count">900</span> seconds</div>
                <button id="continue-button">Yes</button>
            </div>
        </div>
    </body>
    <style>
        .styled-underline {
        	text-decoration:underline red;
            text-underline-offset: 7px;
        }
        .welcome {
            background-color: #006f22;
            border-radius: 0.5em;
            padding: 0.25em;
            text-align: center;
            color: #fff;
            font-weight: 700;
            text-transform: uppercase;
            letter-spacing: .03em;
            font-size: 1.6em;
            line-height: 1em;
            margin-top: 1em;
            text-decoration: none!important;
            margin-bottom: 1em;
        }
        .welcome-flavour-text{
            text-align: center;
            color: #006f22;
        }
        .dotted-hr{
            border: none;
            border-top: 2px dashed green;
            margin: 20px 0;
        }
        .section{
            padding: 20px 30px;
            border-radius: 5px;
            box-shadow: 0 4px 18px rgba(0,0,0,.85);
            background-color: #fff;
            margin-bottom: 30px;
        }
        @media all and (min-width: 40em) {
            .usa-hero__callout {
                max-width:100% !important;
            }
        }
        .plandoc-content span.title {
            display:none;
        }
        @media (min-width: 40em) {
            .usa-section {
                padding-bottom: 2rem !important;
                padding-top: 2rem !important;
            }
        }
        /* Popup styles */
        .popup {
            display: none;
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            background-color: #fff;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);
            padding: 20px;
            text-align: center;
            z-index: 1000;
        }
        .popup-content {
            font-size: 16px;
        }
        #continue-button {
            background-color: #007bff;
            color: #fff;
            padding: 10px 20px;
            border: none;
            cursor: pointer;
        }
    </style>
    <script>
        let idleTimer;
        let popupTimer;
        let timerCount = 900;
        let popupCount = 10;
        const timerCountElement = document.getElementById('timer-count');
        const popupCountElement = document.getElementById('popup-timer-count');
        // Function to reset the timer
        function resetTimer() {
            clearTimeout(idleTimer);
            timerCount = 900;
            updateTimerDisplay();
            startTimer();
        }
        // Function to start the timer
        function startTimer() {
            idleTimer = setInterval(decrementTimer, 1000); // Update timer every 1 second (1000 milliseconds)
        }
        // Function to increment the timer count and update the display
        function decrementTimer() {
            timerCount--;
            updateTimerDisplay();
            if (timerCount <= 0) {
                // Show the session expiration popup when the timer reaches 0
                showPopup();
                if (timerCount < 1) {
                    clearInterval(idleTimer);
                }
            }
        }
        // Function to update the timer display
        function updateTimerDisplay() {
        if (timerCountElement)
            timerCountElement.textContent = timerCount;
        }
        // Function to show the popup
        function showPopup() {
            const popup = document.getElementById('session-expiration-popup');
            popup.style.display = 'block';
            startPopupTimer();
        }
        // Function to start the popup timer
        function startPopupTimer() {
            popupCountElement.textContent = popupCount;
            popupTimer = setInterval(decrementPopupTimer, 1000); // Update popup timer every 1 second (1000 milliseconds)
        }
        // Function to decrement the popup timer
        function decrementPopupTimer() {
            popupCount--;
            popupCountElement.textContent = popupCount;
            if (popupCount <= 0) {
                // Hide the popup and reset the timer when popup timer reaches 0
                clearInterval(popupTimer);
                window.location.href = '/liheap-kiosk/';
            }
        }
        // Function to hide the popup and reset the timer
        function hidePopupAndResetTimer() {
            const popup = document.getElementById('session-expiration-popup');
            popup.style.display = 'none';
            resetTimer();
            clearTimeout(popupTimer);
            popupCount = 10;
        }
        // Add event listener for keyboard and mouse events as well as touch events to reset the timer
        document.addEventListener('mousemove', resetTimer);
        document.addEventListener('click', resetTimer);
        document.addEventListener('wheel', resetTimer);
        document.addEventListener('keydown', resetTimer);
        document.addEventListener('keyup', resetTimer);
        document.addEventListener('touchstart', resetTimer);
        document.addEventListener('touchmove', resetTimer);
        document.addEventListener('touchend', resetTimer);
        document.addEventListener('touchcancel', resetTimer);
        // Initialize the timer
        startTimer();
        // Add event listener for the "Yes" button
        const continueButton = document.getElementById('continue-button');
        continueButton.addEventListener('click', hidePopupAndResetTimer);
    </script>

</html>
