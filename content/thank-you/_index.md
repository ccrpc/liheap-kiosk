---
title: Thank you
---

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <style>
            @import url(//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css);
        </style>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
    	<div class="main-content">
        	<img src="./thank-you.jpg" alt="thanks" />
            <i class="fa fa-check main-content__checkmark" id="checkmark"></i>
            <p class="main-content__body" data-lead-id="main-content-body"> You will be redirected to the main page in <span id="countdown">10</span> seconds.</p>
        </div>
        <style>
            .main-content {
                margin: 0 auto;
                max-width: 820px;
                text-align: center;
            }
            .main-content__checkmark {
                font-size: 4.0625rem;
                line-height: 1;
                color: #24b663;
            }
            .main-content__body {
                margin: 20px 0 0;
                font-size: 1rem;
                line-height: 1.4;
            }
            .plandoc-content span.title {
                display:none;
            }
            @media only screen and (min-width: 40em) {
                .main-content__checkmark {
                    font-size: 9.75rem;
                }
                .main-content__body {
                    font-size: 1.25rem;
                }
            }
        </style>
        <script>
            // JavaScript to update the countdown
            var countdownElement = document.getElementById('countdown');
            var countdownValue = 10; // Initial countdown value
            function updateCountdown() {
                countdownValue--;
                if (countdownValue >= 0) {
                    countdownElement.textContent = countdownValue;
                    setTimeout(updateCountdown, 1000); // Update every 1 second
                } else {
                    // Redirect when countdown reaches 0
                    window.location.href = 'https://ccrpc.gitlab.io/liheap-kiosk/';
                }
            }
            // Start the countdown
            updateCountdown();
        </script>
    </body>
</html>
